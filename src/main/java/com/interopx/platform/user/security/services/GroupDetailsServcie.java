package com.interopx.platform.user.security.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.interopx.platform.user.security.entities.Group;
import com.interopx.platform.user.security.model.GroupDto;
import com.interopx.platform.user.security.repository.GroupRepositoy;

@Service
public class GroupDetailsServcie {

	@Autowired
	private GroupRepositoy groupRepo;

	public Group createGroup(GroupDto group) {

		Group groupEntity = new Group();

		// Converting groupDto to Group
		groupEntity = new Group();
		groupEntity.setGroupId(group.getGroupId());
		groupEntity.setGroupName(group.getGroupName());
		groupEntity.setGroupDesc(group.getGroupDesc());
		groupEntity.setGroupEnabled(true);

		return groupRepo.save(groupEntity);
	}

	public List<Group> retrieveAllGroups() {

		return groupRepo.findByGroupEnabled(true);
	}

	public void deleteGroup(Long groupId) {

		groupRepo.deactivateGroup(false, groupId);

	}
}
