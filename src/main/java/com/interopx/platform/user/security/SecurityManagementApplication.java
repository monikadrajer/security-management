package com.interopx.platform.user.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SecurityManagementApplication {
	public static void main(String[] args) {
		SpringApplication.run(SecurityManagementApplication.class, args);
	}
}
